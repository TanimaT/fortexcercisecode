namespace FortCodeExercises.Exercise1
{
    public class Machine
    {
        #region Private Fields for Public Properites
        // change access modifier of machinename and type fields to private, and Renamed with underscore as private field
        private string _machineName = string.Empty;
        // Renamed with underscore as private field
        private int _type = 0;

        // Introduced private fields for property- Description
        private string _description = string.Empty;

        // Introduced private fields for property- Color and TrimColor
        private string _color = string.Empty;
        private string _trimColor = string.Empty;

        #endregion

        #region Public Getter and Setter properties
       
        // Introduced setter to Name property and move get logic to set
        //Renamed name TO machineName to match private field of _machineName
        public string machineName
        {
            get
            {
                return _machineName;
            }
            set
            {
                if (string.IsNullOrEmpty(_machineName)) // replaced below line with inbuilt string.IsNullOrEmpty()
                {
                    _machineName = value;  // Set value from outside ex setMachineName 
                }               

            }
        }

        // Introduced setter to Description property and move get logic to set
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                this._description = value;
            }
        }

        // Introduced setter to Color property and move get logic to set
        public string Color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value; 
            }
        }

        // Introduced setter to Color property and move get logic to set
        public string trimColor
        {
            get
            {
                return trimColor;
            }
            set
            {
                _trimColor = value ; // called function settrimcolor
            }
        }
        #endregion

        #region Private functions to set properties internally
        private void setMachineName()
        {
            if (this._type == 2) this.machineName = "tractor";
            else if (this._type == 0) this.machineName = "bulldozer";
            else if (this._type == 1) this.machineName = "crane";
            else if (this._type == 4) this.machineName = "car";
            else if (this._type == 3) this.machineName = "truck";
        }
       
        private void setDescription()
        {
            //moved speed logic to common function GetSpeed() and called it below
            // Used C# 5.0 feature of string assignment 

            string _descr = "";
            _descr = $" {_color} {_machineName} [{this.getMaxSpeed(getSpeed())} ]";
            //changed signature of below method- getMaxSpeed
            _descr += this.getMaxSpeed(getSpeed()) + "].";
            this.Description = _descr;

        }

        private void setColor()
        {
            if (this._type == 1) Color = "blue";
            else if (this._type == 0) Color = "red";
            else if (this._type == 4) Color = "brown";
            else if (this._type == 3) Color = "yellow";
            else if (this._type == 2) Color = "green";
            else Color = "white";
        }

        private void setTrimColor()
        {
            //replaced multiple calls to function this.isDark() single call
            bool _isdark = this.isDark();
            if (this._type == 1 && _isdark) trimColor = "black";
            else if (this._type == 1 && !_isdark) trimColor = "white";
            else if (this._type == 2 && _isdark) trimColor = "gold";
            else if (this._type == 3 && _isdark) trimColor = "silver";
        }

        //refactored by removing 1st argument of machinetype which can be replaced by private field _type
        // changed access modifier to private
        private int getMaxSpeed(bool noMax = false)
        {
            //removed unused variable absoluteMax            
            var max = 70;
            if (_type == 1 && noMax == false) max = 70;
            else if (noMax == false && _type == 2) max = 60;
            else if (_type == 0 && noMax == true) max = 80;
            else if (_type == 2 && noMax == true) max = 90;
            else if (_type == 4 && noMax == true) max = 90;
            else if (_type == 1 && noMax == true) max = 75;
            return max;
        }
        private bool getSpeed()
        {
            var hasMaxSpeed = true;

            if (this._type == 3) hasMaxSpeed = false;
            else if (this._type == 1) hasMaxSpeed = true;
            else if (this._type == 2) hasMaxSpeed = true;
            else if (this._type == 4) hasMaxSpeed = false;
            return hasMaxSpeed;
        }

        // Moved baseColor field to common function
        private string getBaseColor()
        {
            var baseColor = "white";
            if (this._type == 0) baseColor = "red";
            else if (this._type == 1) baseColor = "blue";
            else if (this._type == 2) baseColor = "green";
            else if (this._type == 3) baseColor = "yellow";
            else if (this._type == 4) baseColor = "brown";
            else baseColor = "white";
            return baseColor;
        }

        //changed to private
        private bool isDark()
        {
            string baseColor = getBaseColor(); //Called basebaseColor field setting from here
            var isDark = false;
            if (baseColor == "red") isDark = true;
            else if (baseColor == "yellow") isDark = false;
            else if (baseColor == "green") isDark = true;
            else if (baseColor == "black") isDark = true;
            else if (baseColor == "white") isDark = false;
            else if (baseColor == "beige") isDark = false;
            else if (baseColor == "babyblue") isDark = false;
            else if (baseColor == "crimson") isDark = true;
            return isDark;
        }

        #endregion
    
    }
}