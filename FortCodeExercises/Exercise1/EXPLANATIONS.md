# FORT Technical Exercise
All refactoring is made with comments. Refer comments againts each function and property .
Described below is complete list of changes:

1. // Introduced private fields for properties- Description, Color and TrimColor

2. // Introduced setter to all property and moved their get logic to set

3. // Changed access modifier of machinename and type fields to private
        // Renamed with underscore as private field
        private string _machineName = string.Empty;
4. // Renamed with underscore as private field
        private int _type = 0;
  
5. // From setter called respective functions for every property like below-
        this.setMachineName
        this.setDescription();
        this.setColor();
        this.setTrimColor();

6. //Moved speed logic to common function GetSpeed() and called it below
7. Changed signature of below method- getMaxSpeed TO getSpeed

8. //replaced multiple calls to function this.isDark() to a single call
9. //refactored getMaxSpeed() by removing 1st argument of machinetype. It is replaced by private field _type
        // changed function's 'access modifier to private

10.  // Moved baseColor field to common function- getBaseColor()
11.    //Changed function isDark to private
    //removed extra variables :
                var machineName = "";  
                var absoluteMax = 70;

12.    // Used C# 5.0 feature of string assignment of description variable in single line
13. Surrounded code in regions